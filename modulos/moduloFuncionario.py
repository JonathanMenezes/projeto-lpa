def cadastrarFuncionario():
    cpf = input("CPF: ")
    nome = input("Nome: ")
    email = input("Email: ")
    senha = input("Senha: ")
    sexo = input("Sexo: ")
    dataNasci = input("Data de Nascimento: ")
    funcao = input("Funçao: ")
    telefone = input("Telefone: ")
    dataInicio = input("Data Inicio: ")
    historico = input("Historico: ")

    arquivoFuncionario = open("projeto/projeto-lpa/arquivos/funcionarios.txt", "a",encoding='utf-8')
    arquivoFuncionario.write(cpf+', ' + nome+ ', ' + email + ', ' +senha + ', '+ sexo + ', ' +dataNasci+', ' + funcao+', '+ telefone+', ' + dataInicio +', '+historico +'\n')
    arquivoFuncionario.close()

def listarFuncionarios():
    arquivoFuncionario = open("projeto/projeto-lpa/arquivos/funcionarios.txt", "r",encoding='utf-8')
    linhas =  arquivoFuncionario.readlines()
    arquivoFuncionario.close()

    for index,linha in enumerate(linhas):
        print(f'{index+1} - {linha}',end="")
    
def alterarFuncionario():
    arquivoFuncionario = open("projeto/projeto-lpa/arquivos/funcionarios.txt", "r",encoding='utf-8')
    linhas = arquivoFuncionario.readlines()
    arquivoFuncionario.close()

    print("Escolha um funcionario para editar!")
    listarFuncionarios()

    op = int(input("Escolha: "))
    arquivoFuncionario = open("projeto/projeto-lpa/arquivos/funcionarios.txt", "w+",encoding='utf-8')

    for index,linha in enumerate(linhas):
        if(index+1 == op):

            cpf = input("CPF: ")
            nome = input("Nome: ")
            email = input("Email: ")
            senha = input("Senha: ")
            sexo = input("Sexo: ")
            dataNasci = input("Data de Nascimento: ")
            funcao = input("Funçao: ")
            telefone = input("Telefone: ")
            dataInicio = input("Data Inicio: ")
            historico = input("Historico: ")

            arquivoFuncionario.write(cpf+', ' + nome+ ', ' + email + ', ' +senha + ', '+ sexo + ', ' +dataNasci+', ' + funcao+', '+ telefone+', ' + dataInicio +', '+historico +'\n')
            continue

        arquivoFuncionario.write(linha)

    arquivoFuncionario.close()

def removerFuncionario():
    arquivoFuncionario = open("projeto/projeto-lpa/arquivos/funcionarios.txt", "r",encoding='utf-8')
    linhas = arquivoFuncionario.readlines()
    arquivoFuncionario.close()

    print("Escolha um funcionario para remover!")
    listarFuncionarios()
    op = int(input("Escolha: "))

    arquivoFuncionario = open("projeto/projeto-lpa/arquivos/funcionarios.txt", "w+",encoding='utf-8')
    for index,linha in enumerate(linhas):
        if (index+1 == op):
            continue
        arquivoFuncionario.write(linha)

    arquivoFuncionario.close()
    
def menu():
    continuar = "S"

    while continuar == "S":
        opcao = int(input("[1] - Cadastrar novo funcuinario \n[2] - Para lista todos os funcionarios \n[3] - Para editar um funcionario \n[4] - Para remover um funcionario \n[5] - Para retorna ao menu principal \n Escolha: "))
        if(opcao == 1):
            cadastrarFuncionario()
        elif(opcao == 2):
            listarFuncionarios()
        elif(opcao == 3):
            alterarFuncionario()
        elif(opcao == 4):
            removerFuncionario()
        elif(opcao == 5):
            break
        continuar = input("[S] - Para continuar no modulo funcionaro, [N] - para sair: ")


menu()
