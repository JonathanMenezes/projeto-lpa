def escolherSetor():
    arquivoSetor = open("projeto/projeto-lpa/arquivos/setor.txt", "r", encoding='utf-8')
    linhasSetor = arquivoSetor.readlines()
    arquivoSetor.close()
    codigo_setor = ""

    print("Escolha o setor")
    for index, linha_setor in enumerate(linhasSetor):
        print(f"{index+1} - {linha_setor}", end="")
    
    op = int(input("Escolha: "))
    codigo_setor = linhasSetor[op-1][0]
    return codigo_setor

def escolherFuncionario():
    #recuperando CPF do funcionario
    arquivoFuncionario = open("projeto/projeto-lpa/arquivos/funcionarios.txt", "r",encoding='utf-8')
    linhas_funcionario = arquivoFuncionario.readlines()
    arquivoFuncionario.close()
    print("Escolha funcionario responsavel")
    for index,linha_fun in enumerate(linhas_funcionario):
        print(f"{index+1} - {linha_fun}", end="")
    
    op = int(input("Escolha um funcionario: "))
    cpf_fun = linhas_funcionario[op-1][0]
    return cpf_fun
    
def cadastrarResponsavel():
    arquivoResponsavel = open("projeto/projeto-lpa/arquivos/responsavel.txt", "a", encoding='utf-8')
    dataInicio = input("Data de início: ")
    cpf_fun = ""
    codigo_setor = ""
    
    cpf_fun = escolherFuncionario()
    codigo_setor = escolherSetor()

    arquivoResponsavel.write(cpf_fun + ', '+ codigo_setor +', '+ dataInicio + '\n')
    arquivoResponsavel.close()
    
def listarResponsveis():
    arquivoResponsvel = open("projeto/projeto-lpa/arquivos/responsavel.txt", "r", encoding='utf-8')
    linhas = arquivoResponsvel.readlines()
    arquivoResponsvel.close()

    for index, linha in enumerate(linhas):
        print(f"{index+1} - {linha}", end="")

def editarResponsavel():
    arquivoResponsvel = open("projeto/projeto-lpa/arquivos/responsavel.txt", "r", encoding='utf-8')
    linhas = arquivoResponsvel.readlines()
    arquivoResponsvel.close()

    print("Escolha o responsável")
    listarResponsveis()
    op = int(input("Escolha: "))
    
    arquivoResponsvel = open("projeto/projeto-lpa/arquivos/responsavel.txt", "w+", encoding='utf-8')
    for index, linha in enumerate(linhas):
        if(index+1 == op):
            dataInicio = input("Data de início: ")
            cpf_fun = ""
            codigo_setor = ""
            
            cpf_fun = escolherFuncionario()
            codigo_setor = escolherSetor()

            arquivoResponsvel.write(cpf_fun + ', '+ codigo_setor +', '+ dataInicio + '\n')
            continue

        arquivoResponsvel.write(linha)
    arquivoResponsvel.close()

def removerResponsvel():
    arquivoResponsvel = open("projeto/projeto-lpa/arquivos/responsavel.txt", "r", encoding='utf-8')
    linhas = arquivoResponsvel.readlines()
    arquivoResponsvel.close()

    print("Escolha o responsável que deseja remover")
    listarResponsveis()
    op = int(input("Escolha: "))

    arquivoResponsvel = open("projeto/projeto-lpa/arquivos/responsavel.txt", "w+", encoding='utf-8')
    for index, linha in enumerate(linhas):
        if(index+1 == op):
            continue
        arquivoResponsvel.write(linha)
    arquivoResponsvel.close



listarResponsveis()